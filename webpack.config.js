var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var jsEntryPath = path.resolve(__dirname, 'js', 'index.js');
var htmlEntryPath = path.resolve(__dirname, 'js', 'index.html');
var buildPath = path.resolve(__dirname, 'dist');

module.exports = {
    devtool: 'inline-source-map',
    entry: {
        index: [
            jsEntryPath,
        ]
    },
    output: {
        filename: 'bundle.js',
        path: buildPath,
        publicPath: ''
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: [
                    {loader: 'babel-loader'},
                ],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {
                        loader: 'css-loader',

                    }

                ]
            },
            {
                test: /\.scss$/,
                use: [
                    {loader: 'style-loader'},
                    {
                        loader: 'css-loader?modules&localIdentName=[name]__[local]___[hash:base64:5]&importLoaders=1',

                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [

                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {loader: 'sass-loader'}
                ],
                exclude: /global.scss/
            },
            {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff"},
            {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff"},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&mimetype=application/octet-stream"},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader"},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&mimetype=image/svg+xml"},
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'url-loader?limit=1024&name=[name]-[hash:8].[ext]!image-webpack-loader',
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: htmlEntryPath
        }),

    ],
    devServer: {
        host: '0.0.0.0',
        port: 3000,
        historyApiFallback: true
    }
}