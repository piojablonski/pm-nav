# PM NAV

A simple journey planner that enables you to enter origin, journey start time and up to four destinations.
The planner will show you the route on the map and some basic information about the distance and arrival time.

#### demo
- [link](http://pmnav.azurewebsites.net/)


## Install with Docker

- Download the image from docker hub `docker pull piojablonski/pmnav_front`
- To run image use
```bash
docker run -p 3000:3000 --name pm-nav piojablonski/pmnav_front
```
- Go to http://localhost:3000

In case the port 3000 is already in use you can change port mapping:
```bash
docker run -p 3001:3000 --name pm-nav piojablonski/pmnav_front
```


## Install using npm
- Clone the repository `git clone https://piojablonski@bitbucket.org/piojablonski/pm-nav.git`
- Run `npm install`

### Running Dev Server
- Run command `npm run watch`
- Go to http://localhost:3000

### Running Production Server
- Build the client with `npm run build`
- run application on localhost port 3000 using `npm run start`
- Go to http://localhost:3000

### Running tests
- Run `npm run test`