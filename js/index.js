import React from "react";
import ReactDOM from 'react-dom';
import App from './components/app.component'
import 'react-datetime/css/react-datetime.css';

const rootEl = document.getElementById('root');
const render = Component =>
        ReactDOM.render(
            <Component />,
            rootEl
        );

render(App);
