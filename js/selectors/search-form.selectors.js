import {createSelector} from 'reselect'
import R from 'ramda';
import moment from 'moment';


const getRouteResultRequest = state => state.map.routeResult.request;

export const getRouteRequestFromRouteResult = createSelector(
    [getRouteResultRequest],
    (request) => {

        let res = { departureTime: moment(), destinations : [''] };

        if (request) {
            let { drivingOptions : { departureTime }, origin, destination, waypoints} = request;

            let destinations = [...R.map(wp=>wp.location, waypoints), destination];

            res = { departureTime, origin, destinations};
        }
        // console.log('getRouteRequestFromRouteResult', res);
        return res;
    }
)