import {createSelector} from 'reselect';
import R from 'ramda';
import moment from 'moment';

const routeResultSelector = state => state.map.routeResult;

export const routeDetailsSelector = createSelector(
    [routeResultSelector],
    routeResult => {
        let result = null;
        if (!R.isEmpty(routeResult)) {
            if (routeResult.routes) {
                const {
                    request :{
                        drivingOptions : {
                            departureTime
                        }
                    },
                    routes : [
                        {
                            legs
                        }
                    ]
                } = routeResult;

                let startTime = moment(departureTime);
                let waypoints = R.map(leg => ({
                    eta: moment(startTime).add(leg.duration.value, 's'),
                    distance: leg.distance.text,
                    endAddress: leg.end_address

                }), legs);


                result = {startTime, waypoints};

            } else {
                result = {message: 'route not found'}
            }

        }
        return result;
    }
)