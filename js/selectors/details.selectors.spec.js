import {routeDetailsSelector} from './details.selectors';
import moment from 'moment';

const mockState = {
    map: {
        routeResult: {
            routes: [ {
                legs: [
                    {
                        distance: {
                            text: '88.9 mi',
                            value: 143102
                        },
                        duration: {
                            text: '1 hour 40 mins',
                            value: 5986
                        },
                        end_address: 'Creynolds Ln, Shirley, Solihull B90 4ER, UK',
                        end_location: {
                            lat: 52.3849484,
                            lng: -1.8021916999999803
                        },
                        start_address: 'York Pl, Bristol BS8 1AH, UK',
                        start_location: {
                            lat: 51.4547441,
                            lng: -2.6124962000000096
                        }
                    },
                    {
                        distance: {
                            text: '308 mi',
                            value: 495115
                        },
                        duration: {
                            text: '5 hours 24 mins',
                            value: 19457
                        },
                        end_address: 'Edinburgh EH2, UK',
                        end_location: {
                            lat: 55.9522236,
                            lng: -3.2003319000000374
                        },
                        start_address: 'Creynolds Ln, Shirley, Solihull B90 4ER, UK',
                        start_location: {
                            lat: 52.3849484,
                            lng: -1.8021916999999803
                        }
                    }
                ]
            }],
            request: {
                origin: 'BS8 1AH',
                destination: 'EH2, UK',
                provideRouteAlternatives: false,
                travelMode: 'DRIVING',
                drivingOptions: {
                    departureTime: '2017-03-06T21:41:28.005Z',
                    trafficModel: 'pessimistic'
                },
                unitSystem: 1,
                region: 'GB',
                waypoints: [
                    {
                        location: 'B90 4ER',
                        stopover: true
                    }
                ]
            }
        }
    }
};

describe('routeDetailsSelector', () => {
    it('should return route not found message', () => {
        const state = {
            map: {
                routeResult: {
                    status: "ZERO_RESULTS"
                }
            }
        };
        const actual = routeDetailsSelector(state);
        const expected = {message: 'route not found'}
        expect(actual).toEqual(expected);
    });
    it('should return null when routeResult is empty', () => {
        const state = {
            map: {
                routeResult: {}
            }
        };
        const actual = routeDetailsSelector(state);
        expect(actual).toBeNull();
    });

    it('should extract details', () => {

        const actual = routeDetailsSelector(mockState);

        let startTime = '2017-03-06T21:41:28.005Z';
        //console.log(actual);
        const expected = {
            startTime : moment(startTime),
            waypoints : [
                {
                    distance : '88.9 mi',
                    endAddress: 'Creynolds Ln, Shirley, Solihull B90 4ER, UK',
                    eta:  moment(startTime).add(5986, 's')
                },
                {
                    distance : '308 mi',
                    endAddress: 'Edinburgh EH2, UK',
                    eta:  moment(startTime).add(19457, 's')
                }
            ]
        };
        expect(actual).toEqual(expected);
    });

});

