import R from 'ramda';
import moment from 'moment';

export const MAP_API_LOAD_SUCCESS = "MAP_API_LOAD_SUCCESS";
export const NEW_ROUTE_REQUEST = "NEW_ROUTE_REQUEST";
export const CALCULATE_ROUTE = "CALCULATE_ROUTE";
export const CLEAR_ROUTE = "CLEAR_ROUTE";


export const updateRouteDetails = (routeRequest) => dispatch => {
    const directionsService = new google.maps.DirectionsService();
    let apiRouteRequest = {
        origin: routeRequest.origin,
        destination: routeRequest.destination,
        provideRouteAlternatives: false,
        travelMode: 'DRIVING',
        drivingOptions: {
            departureTime: routeRequest.departureTime,
            trafficModel: 'pessimistic'
        },
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        region: 'GB'
    }

    const mapToApiWaypoint = w => ({location: w, stopover: true});
    apiRouteRequest.waypoints = R.map(mapToApiWaypoint, routeRequest.waypoints)

    directionsService.route(apiRouteRequest, (routeResult, status) => {
        dispatch({
            type: CALCULATE_ROUTE,
            routeResult
        });
    });


};

export const clearRoute = () => {
    return {type: CLEAR_ROUTE};
}


