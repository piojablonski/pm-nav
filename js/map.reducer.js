import {MAP_API_LOAD_SUCCESS, NEW_ROUTE_REQUEST, CALCULATE_ROUTE, CLEAR_ROUTE} from "./map.actions";

const mapInitialState = {
    isMapApiLoaded: false,
    routeResult: {}
};

export const mapReducer = (state = mapInitialState, action) => {
    switch (action.type) {
        case MAP_API_LOAD_SUCCESS:
            return {
                ...state,
                isMapApiLoaded: true
            };
        case NEW_ROUTE_REQUEST:
            return {
                ...state,
                routeRequest: action.routeRequest
            };
        case CLEAR_ROUTE:
            return {
                ...state,
                routeResult: {}
            };
        case CALCULATE_ROUTE:
            const {routeResult} = action;
            if (routeResult.status == 'OK') {
                return {
                    ...state,
                    routeResult: routeResult
                }
            } else {
                const {status, request} = routeResult;
                return {
                    ...state,
                    routeResult: {status, request}
                }
            }

        default:
            return state;
    }

};


