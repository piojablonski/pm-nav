import React from 'react';
import {connect} from "react-redux";
import {updateRouteDetails, clearRoute} from "../map.actions";
import 'semantic-ui-css/semantic.min.css';
import moment from 'moment';


@connect()
export class SavedSearch extends React.Component {
    clickHandler(e) {
        let routeRequest = {};
        const {dispatch} = this.props;
        if (e.target.name == 'r1') {
            routeRequest = {
                departureTime: moment().add(1, 'day').toDate(),
                origin: 'BS8 1AH',
                destination: 'EH2, UK',
                waypoints: [
                    'B90 4ER'
                ]

            }
        }
        if (e.target.name == 'r2') {
            routeRequest = {
                departureTime: moment().toDate(),
                origin: 'LS9 0AZ',
                destination: 'CT1 2JJ',
                waypoints: [
                    'WF11 0JQ',
                    'DN1 2DS',
                    'NN1 5A'
                ]

            }
        }
        dispatch(updateRouteDetails(routeRequest));
    }



    render() {
        return <div className="ui basic segment">
            <button className="ui button tiny" onClick={this.clickHandler.bind(this)} name="r1">example 1</button>
            <button className="ui button tiny" onClick={this.clickHandler.bind(this)} name="r2">example 2</button>

        </div>
    }
}
