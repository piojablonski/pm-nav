import React from 'react';
import {mount} from 'enzyme';
import {Details} from './details.component';
import renderer from 'react-test-renderer';
import moment from 'moment';

describe('Details component', () => {
    it('render with route data', () => {
        const startTime = '2017-03-06T21:41:28.005Z';
        const testData =
            {
                startTime: moment(startTime),
                waypoints: [
                    {
                        distance: '88.9 mi',
                        endAddress: 'Creynolds Ln, Shirley, Solihull B90 4ER, UK',
                        eta: moment(startTime).add(5986, 's')
                    },
                    {
                        distance: '308 mi',
                        endAddress: 'Edinburgh EH2, UK',
                        eta: moment(startTime).add(19457, 's')
                    }
                ]
            };

        const rendered = renderer.create(
            <Details data={testData}/>
        );
        expect(rendered.toJSON()).toMatchSnapshot();

    }),
    it('render when route not found', () => {
        const testData =
            {
                message: 'route not found'
            };

        const rendered = renderer.create(
            <Details data={testData}/>
        );
        expect(rendered.toJSON()).toMatchSnapshot();

    })
});
