import React from 'react';
import R from 'ramda';


const RoutesTable = ({waypoints}) =>
    <table className="ui very basic striped table">
        <thead>
        <tr>
            <th>address</th>
            <th>eta</th>
            <th>distance</th>
        </tr>
        </thead>
        <tbody>
        {waypoints.map((wp, i) => (
            <tr key={i}>
                <td>{wp.endAddress}</td>
                <td>{wp.eta.format('lll')}</td>
                <td>{wp.distance}</td>
            </tr>)
        )}
        </tbody>
    </table>;

export const Details = ({data, ...props}) => {
    if (!data)
        return null;
    else {
        const output = (data.waypoints && data.waypoints.length > 0)
            ? <RoutesTable waypoints={data.waypoints} />
            : <div {...props}>{data.message}</div>

        return (
            <div {...props} style={{paddingTop:14, paddingBottom:14}}>
                {output}
            </div>
        )


    }
}

Details.propTypes = {
    data: React.PropTypes.object
}


