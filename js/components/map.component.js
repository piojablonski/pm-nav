import React from 'react';
import ReactDOM from 'react-dom';
import R from 'ramda';

export class Map extends React.Component {
    constructor(props) {
        super(props);
    }

    connectMapElement() {
        if (this.mapElem) {
            const mapDOMElem = ReactDOM.findDOMNode(this.mapElem);
            this.map = new google.maps.Map(mapDOMElem, {
                center: {lat: 51.5, lng: 0},
                zoom: 9
            });

            this.directionsDisplay = new google.maps.DirectionsRenderer({
                draggable:false
            });

            this.directionsDisplay.setMap(this.map);
        }
    }

    displayRoute(routeResult) {
        if (routeResult && R.isArrayLike(routeResult.routes)) {
            this.directionsDisplay.setDirections(routeResult);
        } else {
            this.directionsDisplay.setDirections({routes : []});
        }
    }

    // displayRoute(routeResult) {
    //     if (!R.isEmpty(routeResult)) {
    //         this.directionsDisplay = new google.maps.DirectionsRenderer();
    //         this.directionsDisplay.setMap(this.map);
    //         this.directionsDisplay.setDirections(routeResult);
    //     } else {
    //         if (this.directionsDisplay != null) {
    //             this.directionsDisplay.setMap(null);
    //             this.directionsDisplay = null;
    //         }
    //     }
    // }

    componentDidMount() {
        this.connectMapElement();
        // this.calculateRoute(this.props.routeRequest);
    }

    componentWillUpdate(nextProps) {
        this.displayRoute(nextProps.routeResult);
    }

    render() {
        return (
            <div style={{height:'100vh', width:'100%', backgroundColor: 'gray'}} className='map' ref={rm => {
                this.mapElem = rm
            }}></div>
        );
    }
}
Map.propTypes = {
    routeResult: React.PropTypes.object.isRequired
}

