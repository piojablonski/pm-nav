import React from "react";
import {composeWithDevTools} from 'redux-devtools-extension';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {RootContainer} from './root-container.component';
import {mapReducer} from "../map.reducer";
import {reducer as formReducer} from 'redux-form';
import {MAP_API_LOAD_SUCCESS} from "../map.actions";


const reducers = {
    map: mapReducer,
    form: formReducer
};
const store = createStore(combineReducers(reducers), composeWithDevTools(applyMiddleware(thunk)));

window.initMap = function () {
    store.dispatch({type: MAP_API_LOAD_SUCCESS});
};

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Provider store={store}>
            <RootContainer></RootContainer>
        </Provider>
    }

    // render() {
    //     return (
    //         <div>sit</div>
    //     );
    // }
}

