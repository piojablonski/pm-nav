import React from 'react';
import {mount} from 'enzyme';
import {SearchForm} from './search-form.component';
import {createStore, combineReducers} from 'redux';
import {mapReducer} from "../map.reducer";
import {reducer as formReducer} from 'redux-form';
import {Provider} from 'react-redux';


const mockState = {
    map: {
        routeResult: {
            routes: [ {
                legs: [
                    {
                        distance: {
                            text: '88.9 mi',
                            value: 143102
                        },
                        duration: {
                            text: '1 hour 40 mins',
                            value: 5986
                        },
                        end_address: 'Creynolds Ln, Shirley, Solihull B90 4ER, UK',
                        end_location: {
                            lat: 52.3849484,
                            lng: -1.8021916999999803
                        },
                        start_address: 'York Pl, Bristol BS8 1AH, UK',
                        start_location: {
                            lat: 51.4547441,
                            lng: -2.6124962000000096
                        }
                    },
                    {
                        distance: {
                            text: '308 mi',
                            value: 495115
                        },
                        duration: {
                            text: '5 hours 24 mins',
                            value: 19457
                        },
                        end_address: 'Edinburgh EH2, UK',
                        end_location: {
                            lat: 55.9522236,
                            lng: -3.2003319000000374
                        },
                        start_address: 'Creynolds Ln, Shirley, Solihull B90 4ER, UK',
                        start_location: {
                            lat: 52.3849484,
                            lng: -1.8021916999999803
                        }
                    }
                ]
            }],
            request: {
                origin: 'BS8 1AH',
                destination: 'EH2, UK',
                provideRouteAlternatives: false,
                travelMode: 'DRIVING',
                drivingOptions: {
                    departureTime: '2017-03-06T21:41:28.005Z',
                    trafficModel: 'pessimistic'
                },
                unitSystem: 1,
                region: 'GB',
                waypoints: [
                    {
                        location: 'B90 4ER',
                        stopover: true
                    }
                ]
            }
        }
    }
};

function createTestStore(initialState) {
    const reducers = {
        map: mapReducer,
        form: formReducer
    };
    const store = createStore(combineReducers(reducers), initialState);
    return store;
}


describe('search-form initial state', () =>
{
    let subject;
    beforeEach(() => {
        subject = mount(
            <Provider store={createTestStore()}>
                <SearchForm />
            </Provider>
        )
    });
    it('submit button is disabled', () => {

        const submitButton = subject.find(`[type='submit']`);
        expect(submitButton.length).toBe(1);
    });

    it('displays one destination field', () => {
        const elem = subject.find('.destinationsInput');
        expect(elem).toHaveLength(1);
    });
});

describe('search-form filled with 2 destinations', () => {
    let subject;
    beforeEach(() => {
        subject = mount(
            <Provider store={createTestStore(mockState)}>
                <SearchForm />
            </Provider>
        )
    });

    it('submit button is enabled', () => {

        const submitButton = subject.find(`[type='submit']`);
        expect(submitButton.props().disabled).toBe(false);
    });
    it('displays two destination fields', () => {
        const elem = subject.find('.destinationsInput');
        expect(elem).toHaveLength(2);
    });
    it('destination fields have values', () => {
        const elem = subject.find('.destinationsInput');
        expect(elem.at(0).props().value).toBe('B90 4ER');
        expect(elem.at(1).props().value).toBe('EH2, UK');
    });
    it('origin field has value', () =>{
        const elem = subject.find('[name="origin"]');
        expect(elem.props().value).toBe('BS8 1AH');
    });


});