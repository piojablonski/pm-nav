import React from 'react';
import {reduxForm, Field, FieldArray} from 'redux-form';
import {connect} from "react-redux";
import {updateRouteDetails, clearRoute} from "../map.actions";
import R from 'ramda';
import classNames from 'classnames';
import Datetime from 'react-datetime';
import moment from 'moment';
import {getRouteRequestFromRouteResult} from "../selectors/search-form.selectors";
import styles from './search-form.component.scss';


const renderDestinations = ({fields}) => {
    const canAddDestination = fields.length <= 3;

    const shouldShowDeleteButton = fields.length > 1;

    const renderDestination = (dest, idx) =>
        <div className="field" key={idx}>
            <div className={classNames('ui', {'action': shouldShowDeleteButton}, 'input')}>
                <Field className="destinationsInput" placeholder={`type destination ${idx + 1}`} component="input" name={dest}/>
                {shouldShowDeleteButton &&
                <button type="button" className="ui right mini icon button" onClick={() => fields.remove(idx)}><i
                    className="remove icon"/></button>}
            </div>
        </div>;

    return (
        <div className="field">
            <label>destinations</label>
            <div className="ui clearing segment" style={{marginTop: 0, paddingTop: 7 + 19}}>

                {fields.map(renderDestination)}
                <button type="button" disabled={!canAddDestination} className="ui mini right floated basic icon button"
                        onClick={() => fields.push('')}><i className="plus icon"/>
                </button>
            </div>
        </div>
    )
};

const renderDateTime = ({labelText, ...props}) =>
    <div className="field">
        <label>{labelText}</label>
        <Datetime
            {...props.input}
            className={styles.rdt}
            input={false}
            isValidDate={(currentDate, selectedDate) => {
                return currentDate.isAfter(moment().subtract(1, 'd'));
            }}
        />
    </div>;

const renderOrigin = ({placeholder, labelText, ...props}) =>
    <div className="field">
        <label>{labelText}</label>
        <input placeholder={placeholder} {...props.input} />
    </div>;


const validate = values => {
    const errors = {};

    if (!values.origin || values.origin.length < 1) {
        errors.origin = "origin needs at least 1 character";
    }

    if (!values.destinations || values.destinations.length < 0) {
        errors.destinations = 'add at least 1 destination'
    } else {
        let isOneDestinationValid = false;
        const destinationsArrayErrors = [];
        values.destinations.map((val, idx) => {
            if (val.length < 1) {
                destinationsArrayErrors[idx] = "destination needs at least 1 character"
            }
        });
        // if (!isOneDestinationValid) {
        //     errors.destinations = 'add at least one destination should '
        // }
        if (destinationsArrayErrors.length > 0)
            errors.destinations = destinationsArrayErrors;
    }

    // console.log('validate', values, errors);
    return errors;
}

@connect(state => ({
    initialValues: getRouteRequestFromRouteResult(state)
}))
@reduxForm({
    form: 'routeForm',
    enableReinitialize: true,
    keepDirtyOnReinitialize: true,
    validate
})
export class SearchForm extends React.Component {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.clear = this.clear.bind(this);
    }

    submit(formValues) {
        const {dispatch} = this.props;
        // console.log(formValues);

        let depTime = formValues.departureTime ? moment(formValues.departureTime) : moment();
        if (depTime.isBefore(moment()))
            depTime = moment();

        let routeRequest = {
            departureTime: depTime.toDate(),
            origin: formValues.origin,
            destination: R.last(formValues.destinations),
            waypoints: R.dropLast(1, formValues.destinations)
        };
        dispatch(updateRouteDetails(routeRequest))
    }

    clear() {
        const {dispatch} = this.props;
        dispatch(clearRoute());
    }

    render() {
        const {handleSubmit, submitting, invalid} = this.props;
        // console.log('render form', this.props);
        return (
            <div className="ui basic segment">
                <form onSubmit={handleSubmit(this.submit)} className="ui form">
                    <Field name="departureTime" labelText="departure time" component={renderDateTime}/>
                    <Field placeholder="type origin" name="origin" component={renderOrigin} labelText="origin"
                           type="text"/>

                    <FieldArray name="destinations" component={renderDestinations}/>
                    <button className={classNames('ui blue button', {'loading': submitting})} type="submit"
                            disabled={invalid}>plan journey
                    </button>
                    <button className="ui right floated basic button" type="button" onClick={this.clear}>
                        clear
                    </button>
                </form>
            </div>
        )
    }
}


