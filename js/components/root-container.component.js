import React from 'react';
import {connect} from "react-redux";
import {Map} from './map.component';
import {routeDetailsSelector} from "../selectors/details.selectors";
import {Details} from "./details.component";
import 'semantic-ui-css/semantic.min.css';
import style from './root-container.component.scss';
import {SearchForm} from './search-form.component';
import {SavedSearch} from './saved-search.component';
import classNames from 'classnames';

@connect(state => ({
    map: state.map,
    routeDetails: routeDetailsSelector(state)
}))
export class RootContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {map : {isMapApiLoaded, routeResult}, routeDetails} = this.props;
        const renderMap = isMapApiLoaded ? <Map routeResult={routeResult}/> : <span>Loading....</span>;
        return (
            <div className={style.container}>
                <div className={classNames('ui container', style.form_container)}>
                    <SearchForm />
                    <SavedSearch />
                </div>
                <div className={style.right_container}>
                    {renderMap}
                    <Details className={style.details_container} data={routeDetails}/>
                </div>
            </div>
        );
    }
}